package com.rave.stresstapper

import androidx.compose.foundation.Image
import androidx.compose.foundation.layout.Arrangement
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.fillMaxHeight
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.foundation.layout.padding
import androidx.compose.material3.Button
import androidx.compose.material3.ButtonDefaults
import androidx.compose.material3.Text
import androidx.compose.runtime.Composable
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.res.painterResource
import androidx.compose.ui.text.font.FontWeight
import androidx.compose.ui.unit.dp
import androidx.compose.ui.unit.sp
import androidx.lifecycle.ViewModel
import kotlinx.coroutines.flow.MutableStateFlow
import kotlinx.coroutines.flow.StateFlow
import kotlinx.coroutines.flow.update

class MainViewModel : ViewModel() {

    private val _result = MutableStateFlow(0)
    val result: StateFlow<Int> get() = _result

    fun increment() {
        _result.value++
    }

    fun reset() {
        _result.value = 0
    }

    @Composable
    fun stressButton() {
        Button(onClick = {
            increment()
        }) {
            Text(text = "Stress Button")
        }
    }

    @Composable
    fun StressButtonScreen(stressLevel: Int) {
        Column(
            modifier = Modifier
                .fillMaxWidth()
                .fillMaxHeight(),
            horizontalAlignment = Alignment.CenterHorizontally,
            verticalArrangement = Arrangement.Center
        ) {
            Text(
                text = "Stress Level: $stressLevel",
                fontWeight = FontWeight.Bold,
                modifier = Modifier.padding(top = 24.dp, bottom = 8.dp)
            )
            Button(onClick = {
                increment()
            },
                colors = ButtonDefaults.buttonColors(containerColor = Color.Red)) {
                Text(text = "Stress Button")
            }
        }
    }
}